import { useEffect, useState } from 'react';
import { getDatabase, ref, set, push, get, remove } from 'firebase/database'
import app from '../Firebase-config';
import { async } from '@firebase/util';

const WriteComponent = () => {

    let [inputValue1, setInputValue1] = useState('')
    let [inputValue2, setInputValue2] = useState('')
    let [data, setData] = useState([])
    const db = getDatabase(app)
    const savedata = async () => {
        const newDocRef = push(ref(db, 'formations'))
        set(newDocRef, {
            formationName: inputValue1,
            formationDesc: inputValue2,
        }).then(() => {
            fetchData()
            setInputValue1('')
            setInputValue2('')
        }).catch(error => {
            alert("Erreur d'envoi")
        })
    }
    const fetchData = async () => {
        const formationRef = ref(db, "formations")
        const snapshot = await get(formationRef)
        if (snapshot.exists()) {
            let donn = snapshot.val()
            let tempData = Object.keys(donn).map(item=>{
                return {...donn[item], id: item}
            })
            setData(tempData)
        }else setData([])
    }

    const suppression = async (id)=>{
        const refer = ref(db, 'formations/'+id)
        remove(refer).then(()=>{
           fetchData()       
        })
    }

    const edit = (id)=>{
        let dataEdit= data.filter((i=> i.id == id))[0];
        setInputValue1(dataEdit.formationName);
        setInputValue2(dataEdit.formationDesc);
    }
    
    useEffect(() => {
        fetchData()
    }, [])
    return (
        <div className='container mt-3'>
            <div className="card shadow">
                <div className="card-body">
                    <div className='input-group'>
                        <input className='form-control' type="text" placeholder='Formation' 
                        value={inputValue1} onChange={(e) => setInputValue1(e.target.value)} />
                        <button className='btn btn-outline-primary' onClick={savedata}>Ajouter</button>
                    </div>
                    <br />
                    <textarea className='form-control' placeholder='Description' value={inputValue2} 
                    onChange={(e) => setInputValue2(e.target.value)} cols="30" rows="10"></textarea>
                    <br />
                    <hr />
                    <h3 className='text-center'>Liste des formations</h3>
                    <table className='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th className='bg-primary text-light'>Formation</th>
                                <th className='bg-primary text-light'>Description</th>
                                <th className='bg-primary text-light text-center' width={1}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.formationName}</td>
                                        <td>{item.formationDesc}</td>
                                        <td className='d-flex justify-content-center gap-2'>
                                            <button 
                                            onClick={()=>edit(item.id)} 
                                            className='btn btn-outline-warning btn-sm'><i className='fa fa-edit'></i></button>
                                            <button 
                                            onClick={()=>suppression(item.id)} 
                                            className='btn btn-outline-danger btn-sm'><i className='fa fa-trash'></i></button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default WriteComponent;
