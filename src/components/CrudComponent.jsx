import { useEffect, useState } from 'react';
import { db } from '../FirebaseConfig';
import { collection, addDoc, getDocs, deleteDoc, doc, updateDoc } from 'firebase/firestore'

const CrudComponent = () => {

    let [inputValue1, setInputValue1] = useState('')
    let [inputValue2, setInputValue2] = useState('')
    let [data, setData] = useState([])
    let [identUpdate, setIdentUpdate] = useState()
    const dbRef = collection(db, 'crud')
    const savedata = async () => {
        const addData = await addDoc(dbRef, {formationName: inputValue1, formationDesc: inputValue2})
        if (addData) {
            fetchData()
            realoadField()
        }
    }
    const fetchData = async () => {
        const snapshot = await getDocs(dbRef)
        const dataFetch = snapshot.docs.map(item=>({id: item.id , ...item.data()}))
        if (dataFetch) {
            setData(dataFetch)
        }
    }

    const suppression = async (id)=>{
        const ref = doc(db,'crud', id);  
        await deleteDoc(ref)
        fetchData()   
    }

    const realoadField = () =>{
        setIdentUpdate()
        setInputValue1('')
        setInputValue2('')
    }

    const edit = (id)=>{
        let dataEdit= data.find(i=>i.id === id);
        if (dataEdit) {
            setIdentUpdate(dataEdit.id)
            setInputValue1(dataEdit.formationName);
            setInputValue2(dataEdit.formationDesc);
        }
    }

    const updated = async (id)=>{
        const ref = doc(db, 'crud', id)
        const updateData =  {
            formationName : inputValue1,
            formationDesc : inputValue2
        };
        await updateDoc(ref,updateData)
        fetchData();
        setIdentUpdate()
        realoadField()
    }
    
    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className='container mt-3'>
            <div className="card shadow">
                <div className="card-body">
                    <div className='input-group'>
                        <input className='form-control' type="text" placeholder='Formation' 
                        value={inputValue1} onChange={(e) => setInputValue1(e.target.value)} />
                        {!identUpdate
                            ? <button className='btn btn-outline-primary' 
                            onClick={savedata}>Ajouter</button>
                            :<button className='btn btn-outline-primary' 
                            onClick={()=>updated(identUpdate)}>Modifier</button>
                        }
                    </div>
                    <br />
                    <textarea className='form-control' placeholder='Description' value={inputValue2} 
                    onChange={(e) => setInputValue2(e.target.value)} cols="30" rows="10"></textarea>
                    <br />
                    <hr />
                    <h3 className='text-center'>Liste des formations</h3>
                    <table className='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th className='bg-primary text-light'>Formation</th>
                                <th className='bg-primary text-light'>Description</th>
                                <th className='bg-primary text-light text-center' width={1}>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((item, index) => (
                                    <tr key={index}>
                                        <td>{item.formationName}</td>
                                        <td>{item.formationDesc}</td>
                                        <td className='d-flex justify-content-center gap-2'>
                                            <button 
                                            onClick={()=>edit(item.id)} 
                                            className='btn btn-outline-warning btn-sm'><i className='fa fa-edit'></i></button>
                                            <button 
                                            onClick={()=>suppression(item.id)} 
                                            className='btn btn-outline-danger btn-sm'><i className='fa fa-trash'></i></button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default CrudComponent;
