// Import the functions you need from the SDKs you need

import firebase from 'firebase/compat/app'
import { initializeApp } from "firebase/app";
import 'firebase/compat/firestore'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCAKvWI1EoeGSEqwB-Bx_AMkVaEBEQ139g",
  authDomain: "odc-crud-2-fa49d.firebaseapp.com",
  projectId: "odc-crud-2-fa49d",
  storageBucket: "odc-crud-2-fa49d.appspot.com",
  messagingSenderId: "380590904906",
  appId: "1:380590904906:web:11ffbf1a29e0c186a2b8b3"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore()